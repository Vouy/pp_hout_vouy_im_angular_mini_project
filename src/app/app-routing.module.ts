import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BookComponent } from './components/book/book.component';
import { LoginComponent } from './components/login/login.component';
import { authGuard } from './auth.guard';
import { ViewComponent } from './components/view/view.component';
import { FilterBookComponent } from './components/filter-book/filter-book.component';
import { AddNewBookComponent } from './components/add-new-book/add-new-book.component';
import { LogoutComponent } from './components/logout/logout.component';

const routes: Routes = [
  {
    path : '',
    component : HomeComponent
  },
  {
    path : 'book',
    component : BookComponent,
    canActivate: [authGuard]
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path: 'view/:id',
    component: ViewComponent
  },
  {
    path : 'addNewBook',
    component : AddNewBookComponent
  },
  {
    path : 'logout',
    component: LogoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
