import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/services/ibook';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit{
  books : IBook[] = [];
  viewBook !: IBook;
  bookId !: number;
  constructor(private route : ActivatedRoute, private _serviceBook : BookService){}
  ngOnInit(): void {
    this.getBookById();
  }

  getBookById(){
    this.bookId = +this.route.snapshot.paramMap.get('id')!
    this._serviceBook.getBookById(this.bookId).subscribe(
      (e) => this.viewBook = e
    )
  }

}
