import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/services/ibook';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  

  books: IBook[] = [];

  constructor(private _serviceBook: BookService) { }
  ngOnInit(): void {
    this.getAllBookCategory()
  }

  getAllBookCategory() {
    this._serviceBook.getBooks().subscribe(
      res => {
        this.books = res
      }
    )
  }

 
}



