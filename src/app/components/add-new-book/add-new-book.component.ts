import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup , Validators} from '@angular/forms';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/services/ibook';
@Component({
  selector: 'app-add-new-book',
  templateUrl: './add-new-book.component.html',
  styleUrls: ['./add-new-book.component.css']
})
export class AddNewBookComponent implements OnInit{
  bookForm !: FormGroup;
  book : IBook[] = [];
  constructor(private formBuilder: FormBuilder, private _serviceBook : BookService, private http : HttpClient){}
  ngOnInit(): void {
    this.getAllBook()
    this.initForm()
  }

  private initForm(){
    this.bookForm = this.formBuilder.group({
      title : ['', Validators.required],
      author : ['', Validators.required],
      description : ['', Validators.required],
      category : ['', Validators.required],
      photo : ['book1.jpg', Validators.required]
    })
  }

  getAllBook(){
    this._serviceBook.getBooks().subscribe(res => {
      this.book = res
    })
  }

  addBook(){
    let bookObject : IBook = this.bookForm.value
    this._serviceBook.addNewBook(bookObject).subscribe(
      res => {
        console.log(res)
        this.getAllBook()
      }
    )
  }
}
