import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginForm !: FormGroup
  email !: string

  constructor(private router : Router, private formBuilder : FormBuilder ){}
  ngOnInit(): void {
    this.initFrom()
  }

  private initFrom(){
    this.loginForm = this.formBuilder.group({
      email : ['',  [Validators.required]],
      password : ['', [Validators.required]]
    })
  }

  login(){
    // console.log(this.loginForm)
    localStorage.setItem('token', Math.random().toString());
    this.router.navigate(['book']);
  }
}
