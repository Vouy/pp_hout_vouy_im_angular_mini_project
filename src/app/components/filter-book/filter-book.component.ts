import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/services/ibook';

@Component({
  selector: 'app-filter-book',
  templateUrl: './filter-book.component.html',
  styleUrls: ['./filter-book.component.css']
})
export class FilterBookComponent implements OnInit {

  books: IBook[] = [];
  filterBook !: IBook;
  searchText = '';
  buttonClickCategory : boolean = false;
  // @Output() filterBooks :  EventEmitter<IBook> = new EventEmitter<IBook>();
  categoryName !: string | null;
$event: any;
  constructor(private _serviceBook: BookService, private activatedRoute: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.getAllBook()
    this.getBookByCategoryName()
  }

  getAllBook() {
    this._serviceBook.getBooks().subscribe(
      res => this.books = res
    )
  }

  getBookByCategoryName() {
    this.activatedRoute.queryParamMap.subscribe((res) => {
      this.categoryName = res.get('search')
      for (let book of this.books) {
        if (this.categoryName === book.category) {
          // console.log(this.filterBook = book)
          this.filterBook = book
          this.buttonClickCategory = true
          console.log("book:",this.filterBook)
          
        }
      }
      // this.buttonClickCategory = false
    })
  }

  clickCategory(catedoryName : string​) {
    this.router.navigate(['/book'], {
      queryParams: { search: catedoryName }
    })
  }

}
