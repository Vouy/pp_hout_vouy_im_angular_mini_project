import { HttpClient } from '@angular/common/http';
import { Component, DoCheck, Input, OnChanges, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/services/ibook';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit,OnChanges,DoCheck{

  books : IBook[] = [];
  
  @Input() filter !: IBook;
  @Input() clickCategory !: boolean;
  show = false;
  bookForm !: FormGroup;
  newId :any;

  viewBookFormCategory !: IBook; 
  filterCategory !: boolean;
  _default:boolean =true
  _isCategory!:boolean

  constructor(private formBuilder : FormBuilder ,private _serviceBook : BookService, private http : HttpClient){  
    // console.log("this : ",this.filter)
  }
  ngOnInit(): void {
    this.initForm();
    this.getAllBook()

  }

  ngOnChanges(): void {
    this.viewBookFormCategory = this.filter;
    this.filterCategory = this.clickCategory
    // this.getAllBook()

    // console.log("this : ",this.filter)
    // console.log('booL:', this._isCategory)
  }
  ngDoCheck(): void {
    console.log("this : ",this.filter)
    if(this.filter !== undefined){
      this._default=false
    }
  }

  private initForm() {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      description : ['', Validators.required],
      photo : ['',Validators.required]
    });
  }

  getAllBook(){
    // this.filterCategory = true
    this._serviceBook.getBooks().subscribe(
      (e) => {
        this.books = e;
        // console.log("e : ",e)

        // if(this.filter != undefined){
        //   console.log("this.filter : ",this.filter)
        // }

        console.log("me :",this.filter)
      }
      
    )
  }

  removeBook(bookId : number){
    this._serviceBook.deleteBook(bookId).subscribe()
    this.getAllBook()
  }    

  openPopup(book : IBook, id : any){
    this.show = true
    this.newId = id
    this.bookForm.setValue({
      title : book.title,
      author : book.author,
      description : book.description,
      photo : book.photo
    })

  }

  updateArticle(){
    this._serviceBook.editArticle(this.newId , this.bookForm.value).subscribe(
      res => console.log("res : ",res)
    )
    this.getAllBook()

    this.show = false
     
  }

  closePopup(){
    this.show = false
  }


}
