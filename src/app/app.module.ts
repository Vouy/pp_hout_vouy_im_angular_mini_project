import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { BookComponent } from './components/book/book.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './services/data.service';
import { ViewComponent } from './components/view/view.component';
import { EditPopupComponent } from './components/edit-popup/edit-popup.component';
import { ValidateEmailDirective } from './directive/validate-email.directive';
import { FilterPipe } from './pipes/filter.pipe';
import {FormsModule} from '@angular/forms';
import { FilterBookComponent } from './components/filter-book/filter-book.component';
import { AddNewBookComponent } from './components/add-new-book/add-new-book.component';
import { ValidatePasswordDirective } from './directive/validate-password.directive';
import { LogoutComponent } from './components/logout/logout.component';
import { Nav2Component } from './components/nav2/nav2.component'
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    LoginComponent,
    BookComponent,
    BookListComponent,
    ViewComponent,
    EditPopupComponent,
    ValidateEmailDirective,
    FilterPipe,
    FilterBookComponent,
    AddNewBookComponent,
    ValidatePasswordDirective,
    LogoutComponent,
    Nav2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(DataService),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
