export interface IBook {
    id: number;
    title: string;
    author: string;
    description : string;
    category : string;
    photo : string;
}
