import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IBook } from './ibook';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class BookService {
  BASE_URL = 'api/books';
  constructor(private http: HttpClient) { }

  getBooks() : Observable<IBook[]>{
    return this.http.get<IBook[]>(this.BASE_URL);
  }

  deleteBook(id : number) : Observable<IBook>{
    return this.http.delete<IBook>(this.BASE_URL +'/'+ id)
  }

  getBookById(id : number) : Observable<IBook>{
    return this.http.get<IBook>(this.BASE_URL +'/'+id)
  }

  editArticle(id : any , book : IBook) : Observable<IBook>{
    return this.http.put<IBook>(this.BASE_URL +'/'+id,{
      id : id,
      title: book.title,
      author: book.author,
      description : book.description,
      photo : book.photo
    });
  }

  // getBookByCategoryName(categoryName : string | null) : Observable<IBook[]>{
  //   return this.http.get<IBook[]>(this.BASE_URL + "/"+ categoryName)
  // }

  addNewBook(book : IBook): Observable<IBook>{
    return this.http.post<IBook>(this.BASE_URL, book);
  }
}
