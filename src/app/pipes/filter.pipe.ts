import { Pipe, PipeTransform } from '@angular/core';
import { IBook } from '../services/ibook';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  // transform(items: any[], searchText: string): any[]{
  //   if (!items) return [];
  //   if (!searchText) return items;
  //   searchText = searchText.toString().toLowerCase();
  //   return items.filter((it) => {
  //     return it.name.toLowerCase().startsWith(searchText);
  //   });
  // }

  transform(value:IBook[], searchText:string) {       
    if(!searchText)
    return value;
    let filteredValues:any=[];      
    for(let i=0;i<value.length;i++){
        if(value[i].category.toLowerCase().includes(searchText.toLowerCase())){
            filteredValues.push(value[i]);
        }
    }
    return filteredValues;
}

}
