import { Directive } from '@angular/core';
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
@Directive({
  selector: '[appValidatePassword]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePasswordDirective,
    multi: true
  }]
})
export class ValidatePasswordDirective implements Validator{
// /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/

  constructor() { }
  validate(control: FormControl): { [key: string]: any } | null {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    const valid = passwordRegex.test(control.value);
    return valid ? null : { invalidPassword: true };
  }
}
