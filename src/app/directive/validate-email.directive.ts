import { Directive} from '@angular/core';
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
@Directive({
  selector: '[appValidateEmail]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateEmailDirective,
    multi: true
  }]
})
export class ValidateEmailDirective implements Validator {

  validate(control: FormControl): { [key: string]: any } | null {
    const emailRegex = /^[a-zA-Z]+[_]+[0-9]+@[a-zA-z]+[.]+[a-z]+$/;
    const valid = emailRegex.test(control.value);
    return valid ? null : { invalidEmail: true };
  }
}
